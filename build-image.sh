#!/bin/bash

source third_party/common
echo "Image: "$IMAGE_NAME

echo "Waiting for device..."
adb wait-for-device

adb push third_party/install_docker.sh .
adb shell ./install_docker.sh

echo "Checking for docker support..."
DOCKER_ACTIVE=$(adb shell systemctl is-active docker-start)
if [[ $DOCKER_ACTIVE == *active* ]]; then
	echo "-> Docker support is running on target."
else
	echo "-> Docker support not detected, running docker support on target..."
	adb shell voxl-configure-docker-support.sh
fi

echo "Building image: $IMAGE_NAME"
adb shell mkdir -p $TARGET_DIR
adb push Dockerfile $TARGET_DIR
adb push third_party/voxl2_takeoff_land.py $TARGET_DIR

adb shell docker build -t $IMAGE_NAME $TARGET_DIR
adb shell rm -rf $TARGET_DIR/voxl2_takeoff_land.py
adb push third_party/run-docker.sh $TARGET_DIR
